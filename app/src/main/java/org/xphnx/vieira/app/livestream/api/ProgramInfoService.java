package org.xphnx.vieira.app.livestream.api;


import org.xphnx.vieira.app.livestream.api.model.ShowResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface ProgramInfoService {

	@GET("shows/{channelName}")
	Call<ShowResponse> getShows(@Path("channelName") String channelName);

}
