package org.xphnx.vieira.app.mediathek.api.result;


@SuppressWarnings({"CanBeFinal", "unused"})
public class MediathekAnswer {

	public String err;
	public MediathekAnswerResult result;

}
