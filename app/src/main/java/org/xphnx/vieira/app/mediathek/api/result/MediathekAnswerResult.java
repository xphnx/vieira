package org.xphnx.vieira.app.mediathek.api.result;


import java.util.List;

import org.xphnx.vieira.app.mediathek.model.MediathekShow;

@SuppressWarnings({"CanBeFinal", "unused"})
public class MediathekAnswerResult {

	public List<MediathekShow> results;

}
